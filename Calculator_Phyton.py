def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def divide(x, y):
    return x / y


i = 9
while i:
    print("Select operation.")
    print("1.Add")
    print("2.Subtract")
    print("3.Multiply")
    print("4.Divide")
    print("C. Clear")
    print("Stop - To stop the calculator")
    choice = input("Enter choice(1/2/3/4/C/Stop):")
    if choice == "C":
        continue
    elif choice == "c":
        continue
    elif choice == "Stop":
        print("Program terminated")
        break
    num1 = float(input("Enter first number: "))
    if num1 < 0:
        print("Error")
        continue
    num2 = float(input("Enter second number: "))
    if num2 <= 0:
        print("Error")
        continue
    if choice == '1':
        print(num1, "+", num2, "=", add(num1, num2))
    elif choice == '2':
        print(num1, "-", num2, "=", subtract(num1, num2))
    elif choice == '3':
        print(num1, "*", num2, "=", multiply(num1, num2))
    elif choice == '4':
        print(num1, "/", num2, "=", divide(num1, num2))
    else:
        print("Invalid input")
